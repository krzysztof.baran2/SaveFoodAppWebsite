# KrzysztofPortfolio - Save Food App website

My name is Krzysztof Baran and I am a second year Data Science student at Dublin City University.
This is the project for the Save Food App website.

* This website was created as part of a continuous assessment project I had.
* This involved ideating an app, designing a poster and finally a website to advertise our app.
* This was a group project and as such I don't take credit for everything.
* I had been the main designer of the website.
* While coding this website, my HTML and CSS skills have improved very much.
* The website portrays fictitious statements and is only intended to showcase my static website design skill.

The website is available at [Save Food App](https://krzysztofportfolio.vercel.app/).

